﻿using BackendNetCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BackendNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IConfiguration _config;
        public DepartmentController(IConfiguration config)
        {
            _config = config;
        }
        [HttpGet]
        public JsonResult Get()
        {
            SqlDataReader myReader;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("SQLAuth")))
            {
                conn.Open();

                String query = "SELECT id, Name FROM dbo.Department";

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);
                    conn.Close();
                }
            }
            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Department dep)
        {
            SqlDataReader myReader;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("SQLAuth")))
            {
                conn.Open();

                String query = "INSERT INTO dbo.Department VALUES (@Name)";

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@Name", dep.Name);
                    myReader = command.ExecuteReader();
                    table.Load(myReader);
                    conn.Close();
                    //commentaaa
                }
            }
            return new JsonResult("Added Succesfully");
        }
        [HttpPut]
        public JsonResult Put(Department dep)
        {
            SqlDataReader myReader;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("SQLAuth")))
            {
                conn.Open();

                String query = "UPDATE dbo.Department " +
                    "SET Name=@Name WHERE id=@id";

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@id", dep.id);
                    command.Parameters.AddWithValue("@Name", dep.Name);
                    myReader = command.ExecuteReader();
                    table.Load(myReader);
                    conn.Close();
                }
            }
            return new JsonResult("Updated Succesfully");
        }
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            SqlDataReader myReader;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("SQLAuth")))
            {
                conn.Open();

                String query = "DELETE FROM DBO.DEPARTMENT WHERE id = @id";

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@id", id);
                    myReader = command.ExecuteReader();
                    table.Load(myReader);
                    conn.Close();

                }
            }
            return new JsonResult("Deleted Succesfully");
        }

    }
}